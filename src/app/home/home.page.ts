import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare var google;

@Component({
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class MarkerPage implements OnInit, AfterViewInit {
  @ViewChild('map', { static: false }) mapNativeElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  directionForm: FormGroup;
  currentLocation: any = {
    lat: 0,
    lng: 0
  };
  autocomplete: any;
  GoogleAutocomplete: any;
  GooglePlaces: any;
  geocoder: any;
  autocompleteItems: any;
  nearbyItems: any = new Array<any>();
  loading: any;
  latitude: any;
  longitude: any;
  map: any;

  FixTimeMarkerArray: any = [];
  OpenTimeMarkerArray: any = [];
  FixlatlngArray: any = [];
  OpenlatlngArray: any = [];

  constructor(private fb: FormBuilder, private zone: NgZone, private loadingCtrl: LoadingController, private geolocation: Geolocation) {
    this.createDirectionForm();


    this.geocoder = new google.maps.Geocoder;
    let elem = document.createElement("div")
    this.GooglePlaces = new google.maps.places.PlacesService(elem);
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = {
      input: ''
    };
    this.autocompleteItems = [];
    this.loading = this.loadingCtrl.create();
  }

  ngOnInit() {
  }

  createDirectionForm() {
    this.directionForm = this.fb.group({
      destination: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      this.map = new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: this.currentLocation,
        fullscreenControl: true,
        zoomControl: false,
        streetViewControl: true,
        clickableIcons: false,
        zoom: 16

      });
      const fix1 = { lat: 10.369380, lng: 123.913063 };
      const fix2 = { lat: 10.33055853930485, lng: 123.93247418984379 };
      const fix3 = { lat: 10.353825413988048, lng: 123.93438153905868 };
      const open1 = { lat: 10.365310, lng: 123.927730 };
      const open2 = { lat: 10.327970, lng: 123.941109 };
      const open3 = { lat: 10.335640, lng: 123.914040 };
      // this.latlngArray={posi1},{posi2},{posi3};
      this.FixlatlngArray.push(fix1);
      this.FixlatlngArray.push(fix2);
      this.FixlatlngArray.push(fix3);
      this.OpenlatlngArray.push(open1);
      this.OpenlatlngArray.push(open2);
      this.OpenlatlngArray.push(open3);
      // this.latlngArray={10.369380,123.913063};
      // this.latlngArray=10.33055853930485,123.93247418984379;
      // this.latlngArray=10.353825413988048,123.93438153905868;
      // for (var i = 0; i < 3; i++) {

      //   console.log(this.FixlatlngArray[i]);
      //   //  console.log(posi1);
      // }

      // for (var i = 0; i < 3; i++) {

      //   console.log(this.OpenlatlngArray[i]);
      //   //  console.log(posi1);
      // }

      const pos = {
        lat: this.latitude,
        lng: this.longitude
      };

      const icon = {
        url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
        //url: 'assets/icon/driver.png', // image url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      };

      // const FixTimeIcon = {
      //   url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
      //   //url: 'assets/icon/driver.png', // image url
      //   scaledSize: new google.maps.Size(50, 50), // scaled size
      // };

      // const OpenTimeIcon = {
      //   url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
      //   //url: 'assets/icon/driver.png', // image url
      //   scaledSize: new google.maps.Size(50, 50), // scaled size
      // };


      // for (var i = 0; i < 3; i++) {

      //   this.FixTimeMarkerArray[i] = new google.maps.Marker(
      //     {
      //       position: this.FixlatlngArray[i],
      //       map: this.map,
      //       title: 'Fix Time Parking Space Locations',
      //       icon: FixTimeIcon
      //     }
      //   );

      // }



      // for (var i = 0; i < 3; i++) {

      //   this.OpenTimeMarkerArray[i] = new google.maps.Marker(
      //     {
      //       position: this.OpenlatlngArray[i],
      //       map: this.map,
      //       title: 'Open Time Parking Space Locations!',
      //       icon: OpenTimeIcon
      //     }
      //   );

      // }

      this.directionsDisplay.setMap(this.map);

      this.map.setCenter(pos);

      const DriverLocationMarker = new google.maps.Marker(
        {
          position: pos,
          map: this.map,
          title: 'Drivers Location!',
          icon: icon
        }
      );

      //  const  marker2 = new google.maps.Marker(
      //   {
      //   draggable: true,
      //   map: this.map,
      //   animation: google.maps.Animation.DROP,
      //   position: {lat: 10.369380, lng: 123.913063},
      //   icon: icon
      //   }
      //   );

      const contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">Rate: 10php/hr</h1>' +
        '<div id="bodyContent">' +
        '<img src="assets/icon/user.png" width="150">' +
        '<h3>Description: 4 wheels, open time</h3>' +
        '</div>' +
        '</div>';

      const infowindow = new google.maps.InfoWindow({
        content: contentString + '<ion-button onclick="myFunction()">RESERVE NOW</ion-button>',
        maxWidth: 350
      });

      for (var i = 0; i < 3; i++) {
        const FixTimeMarker = this.FixTimeMarkerArray[i];
        FixTimeMarker.addListener('click', function () {
          infowindow.open(this.map, FixTimeMarker);
          // console.log(marker.getPosition().lat());
          // console.log(marker.getPosition().lng());
        });
      }

      for (var i = 0; i < 3; i++) {
        const OpenTimeMarker = this.OpenTimeMarkerArray[i];
        OpenTimeMarker.addListener('click', function () {
          infowindow.open(this.map, OpenTimeMarker);
          // console.log(marker.getPosition().lat());
          // console.log(marker.getPosition().lng());
        });
      }

      // marker2.addListener('click', function() {
      //   infowindow.open(this.map, marker2);
      //   console.log(marker2.getPosition().lat());
      //   console.log(marker2.getPosition().lng());
      // });

    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  updateSearchResults() {
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
      (predictions, status) => {
        this.autocompleteItems = [];
        if (predictions) {
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItems.push(prediction);
            });
          });
        }
      });
  }
  markers: any;
  selectSearchResult(item) {
    this.autocompleteItems = [];

    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      if (status === 'OK' && results[0]) {
        let position = {
          lat: results[0].geometry.location.lat,
          lng: results[0].geometry.location.lng
        };
        // let marker = new google.maps.Marker({
        //    position: results[0].geometry.location,
        //   map: this.map,
        // });
        //  this.markers.push(marker);
        this.map.setCenter(results[0].geometry.location);
      }
    })
  }

  FixTimeIcon = {
    url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
    //url: 'assets/icon/driver.png', // image url
    scaledSize: new google.maps.Size(50, 50), // scaled size
  };

  FixTimeMarkerFunc() {
    let fix: boolean = false;
    if (fix === false) {
      fix = true;
      for (var i = 0; i < 3; i++) {
        this.FixTimeMarkerArray[i] = new google.maps.Marker(
          {
            position: this.FixlatlngArray[i],
            map: this.map,
            title: 'Fix Time Parking Space Locations',
            icon: this.FixTimeIcon
          }
        )
      }
      // show Modal
    } else {
      fix = false;
      // hide Modal
    }
  }

  OpenTimeIcon = {
    url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
    //url: 'assets/icon/driver.png', // image url
    scaledSize: new google.maps.Size(50, 50), // scaled size
  };

  OpenTimeMarkerFunc() {
    let open: boolean = false;
    if (open === false) {
      open = true;
      for (var i = 0; i < 3; i++) {
        this.OpenTimeMarkerArray[i] = new google.maps.Marker(
          {
            position: this.OpenlatlngArray[i],
            map: this.map,
            title: 'Open Time Parking Space Locations!',
            icon: this.OpenTimeIcon
          }
        )
      }
      // show Modal
    } else {
      open = false;
      // hide Modal
    }

  }


}